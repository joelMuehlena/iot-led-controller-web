export const fetchTimeout = async (
  ms: number,
  url: string,
  options?: RequestInit
): Promise<Response> => {
  const controller = new AbortController();
  const timer = setTimeout(() => {
    controller.abort();
  }, ms);

  const response = await fetch(url, { ...options, signal: controller.signal });
  clearTimeout(timer);

  return response;
};
