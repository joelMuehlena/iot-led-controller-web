import { useEffect, useRef, useState } from "react";
import { LEDSocket, LEDSocketCallbackFn, LEDSocketState } from "./LEDSocket";
import { socketConnections } from "./socketConfig";

export const useWebSocket = (url: string, port: number = 81) => {
  const socket = useRef<LEDSocket>();

  const [wsState, setWsState] = useState<LEDSocketState>(
    socket.current?.state ?? LEDSocketState.CLOSED
  );

  useEffect(() => {
    const int = setInterval(() => {
      if (socket.current) {
        setWsState(socket.current.state);
      }
    }, 1 * 1000);

    return () => {
      clearInterval(int);
    };
  }, []);

  useEffect(() => {
    if (!socketConnections.has(`${url}:${port}`)) {
      socketConnections.set(`${url}:${port}`, new LEDSocket(url, port));
    }

    const conn = socketConnections.get(`${url}:${port}`);
    socket.current = conn;
  }, [url, port]);

  const send = (channel: string, msg: string | object) => {
    socket.current?.send(channel, msg);
  };

  const subsribe = (channel: string, cb: LEDSocketCallbackFn) => {
    socket.current?.subscribe(channel, (data) => {
      cb(data);
    });
  };

  return { send, subsribe, state: wsState };
};
