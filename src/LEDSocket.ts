interface SocketMessage {
  channel: string;
  isBroadcast: boolean;
  data: any;
}

export type LEDSocketCallbackFn = (data: SocketMessage) => void;

export enum LEDSocketState {
  ERROR,
  OPEN,
  CLOSED,
}

export const stringFromLEDConnectState = (state: LEDSocketState): string => {
  switch (state) {
    case LEDSocketState.OPEN:
      return "OPEN";
    case LEDSocketState.CLOSED:
      return "CLOSED";
    case LEDSocketState.ERROR:
      return "ERROR";
    default:
      return "error";
  }
};

export class LEDSocket {
  public readonly RECONNECT_OPTIONS = { interval: 1000, times: 5 };

  private socket: WebSocket;
  private subscriptions: Map<string, Array<LEDSocketCallbackFn>>;
  private _isOpen: boolean = false;
  private _hasErorr: boolean = false;
  private _state: LEDSocketState = LEDSocketState.CLOSED;

  constructor(url: string, port: number = 81) {
    this.subscriptions = new Map();
    this.socket = new WebSocket(`ws://${url}:${port}`);
    this.receivecMsg = this.receivecMsg.bind(this);
    this.socket.onmessage = this.receivecMsg;
    this.socket.onopen = () => {
      this._isOpen = true;
      this._state = LEDSocketState.OPEN;
    };

    this.socket.onclose = () => {
      this._state = LEDSocketState.CLOSED;
    };

    this.socket.onerror = (error) => {
      console.log("WS error", error);
      this._hasErorr = true;
      this.subscriptions.forEach((subscriber) => {
        subscriber.forEach((cb) =>
          cb({ channel: "ERROR_CB", isBroadcast: false, data: { error } })
        );
      });
    };
  }

  public get isConnected(): boolean {
    return this._isOpen;
  }

  public get hasError(): boolean {
    return this._hasErorr;
  }

  public get state(): LEDSocketState {
    return this._state;
  }

  private receivecMsg(msg: MessageEvent<any>) {
    let data: SocketMessage;

    try {
      data = JSON.parse(msg.data);
    } catch (err) {
      data = msg.data;
    }

    console.log(data);

    if (this.subscriptions.has(data.channel)) {
      this.subscriptions.get(data.channel)!.forEach((cbFn) => cbFn(data));
    }
  }

  public subscribe(channel: string, cb: LEDSocketCallbackFn) {
    if (this.subscriptions.has(channel)) {
      this.subscriptions.get(channel)?.push(cb);
    } else {
      this.subscriptions.set(channel, [cb]);
    }
  }

  public send(channel: string, data: any) {
    let msg: SocketMessage = { channel, data, isBroadcast: false };
    this.socket.send(JSON.stringify(msg));
  }
}
