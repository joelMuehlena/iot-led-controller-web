import {
  ChangeEvent,
  FunctionComponent,
  useContext,
  useEffect,
  useState,
} from "react";
import { ColorResult, HuePicker } from "react-color";
import { useWebSocket } from "../../useWebSocket";
import { BrightnessController } from "../common/BrightnessController";
import { RangeSlider } from "../common/RangeSlider";
import { ModeContext, ModexContextI } from "./ModeControlProvider";

export const FadeLedModeControl: FunctionComponent = () => {
  const { config, device } = useContext<ModexContextI>(ModeContext);

  const socket = useWebSocket(device.ip, device.port);

  useEffect(() => {
    if (config.currentColor) {
      setFromColor(config.currentColor);
    }
  }, [config.currentColor]);

  const [fromColor, setFromColor] = useState<string>("#FFE400");

  const [toColor, setToColor] = useState<string>("#FF00E8");

  const [speed, setSpeed] = useState<number>(6);

  const onChangeColorTo = (color: ColorResult) => {
    setToColor(color.hex);
    socket.send("config", { toColor });
  };

  const onChangeColorFrom = (color: ColorResult) => {
    setFromColor(color.hex);
    socket.send("config", { fromColor });
  };

  const onSpeedChange = (e: ChangeEvent<HTMLInputElement>) => {
    setSpeed(parseInt(e.currentTarget.value));
    socket.send("config", { period: speed });
  };

  return (
    <div>
      FadeControls
      <BrightnessController initValue={config.brightness ?? 50} />
      <h3>Colors</h3>
      <h4>From</h4>
      <HuePicker color={fromColor} onChange={onChangeColorFrom} />
      <h4>To</h4>
      <HuePicker color={toColor} onChange={onChangeColorTo} />
      <h4>Speed</h4>
      <RangeSlider value={speed} min={0.5} max={30} change={onSpeedChange} />
    </div>
  );
};
