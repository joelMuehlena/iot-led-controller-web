import {
  ChangeEvent,
  FunctionComponent,
  useContext,
  useEffect,
  useState,
} from "react";
import { ColorResult, HuePicker } from "react-color";
import { useWebSocket } from "../../useWebSocket";
import { BrightnessController } from "../common/BrightnessController";
import { ModeContext, ModexContextI } from "./ModeControlProvider";

export const StaticLedModeControl: FunctionComponent = () => {
  const { config, device } = useContext<ModexContextI>(ModeContext);

  const socket = useWebSocket(device.ip, device.port);

  useEffect(() => {
    if (config.currentColor) {
      setColor(config.currentColor);
    }
  }, [config.currentColor]);

  const [color, setColor] = useState<string>(config.currentColor ?? "#1AD2C1");

  const onColorChange = (
    color: ColorResult,
    _: ChangeEvent<HTMLInputElement>
  ) => {
    setColor(color.hex);
    socket.send("config", { color: color.hex });
  };

  return (
    <div>
      StaticControls
      <BrightnessController initValue={config.brightness ?? 50} />
      <HuePicker color={color} onChange={onColorChange} />
    </div>
  );
};
