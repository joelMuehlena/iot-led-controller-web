import React, {
  Children,
  createContext,
  FunctionComponent,
  isValidElement,
} from "react";
import { Device } from "../DeviceList/DeviceList";
import { InitialModeConfig } from "./ModeControl";

interface Props {
  currentMode: string;
  currentDevice: Device;
  config: Partial<InitialModeConfig>;
}

export interface ModexContextI {
  mode: string;
  device: Device;
  config: Partial<InitialModeConfig>;
}

export const ModeContext = createContext<ModexContextI>({
  mode: "",
  device: { ip: "0.0.0.0", port: 0 },
  config: {},
});

const checkForValid = (children: any, currentMode: string): boolean => {
  if (isValidElement(children)) {
    if (currentMode === (children as any).props.modeName) {
      return true;
    }
  }
  return false;
};

export const ModeControlProvider: FunctionComponent<Props> = ({
  currentMode: mode,
  currentDevice: device,
  config,
  children,
}) => {
  return (
    <ModeContext.Provider value={{ mode, device, config }}>
      {Children.map(children, (child) => {
        if (checkForValid(child, mode)) return child;
      })}
    </ModeContext.Provider>
  );
};
