import React, { FunctionComponent, isValidElement, ReactNode } from "react";

export interface InitialModeConfig {
  brightness: number;
  currentColor: string;
  currentMode: string;
  controllerState: "on" | "off";
  otherData?: any;
}

interface Props {
  modeName: string;
  component: ReactNode;
}

export const ModeControl: FunctionComponent<Props> = ({ component }) => {
  return <>{isValidElement(component) && component}</>;
};
