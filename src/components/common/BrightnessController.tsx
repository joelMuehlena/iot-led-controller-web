import {
  ChangeEvent,
  FunctionComponent,
  useContext,
  useEffect,
  useState,
} from "react";
import { useWebSocket } from "../../useWebSocket";
import { ModeContext, ModexContextI } from "../Modes/ModeControlProvider";
import { RangeSlider } from "./RangeSlider";

interface Props {
  onChangeBrightness?: (e: ChangeEvent<HTMLInputElement>) => void;
  initValue?: number;
}

export const BrightnessController: FunctionComponent<Props> = ({
  onChangeBrightness,
  initValue,
}) => {
  const [value, setValue] = useState<number>(50);

  useEffect(() => {
    console.log(initValue);
    if (initValue) setValue(initValue);
  }, [initValue]);

  const { device } = useContext<ModexContextI>(ModeContext);

  const socket = useWebSocket(device.ip, device.port);

  const localChange = (e: ChangeEvent<HTMLInputElement>) => {
    setValue(parseInt(e.currentTarget.value));
    if (onChangeBrightness) {
      onChangeBrightness(e);
      return;
    } else {
      socket.send("config", { brightness: e.currentTarget.value });
    }
  };

  return <RangeSlider label="Brigtness" change={localChange} value={value} />;
};
