import { ChangeEvent, FunctionComponent } from "react";

interface Props {
  label?: string;
  value: number;
  change: (e: ChangeEvent<HTMLInputElement>) => void;
  min?: number;
  max?: number;
}

export const RangeSlider: FunctionComponent<Props> = ({
  label,
  change,
  value,
  min = 0,
  max = 100,
}) => {
  return (
    <>
      {label && <label>{label}</label>}
      <input
        type="range"
        onChange={change}
        value={value}
        min={min}
        max={max}
      ></input>
    </>
  );
};
