import { FunctionComponent, useCallback, useEffect, useState } from "react";
import { fetchTimeout } from "../../fetchTimeout";

import styles from "./deviceList.module.sass";

export interface Device {
  ip: string;
  port: number;
}

interface Props {
  devices: Array<Device>;
  onclick: (device: Device) => void;
}

interface DeviceState {
  device: Device;
  state: "online" | "offline";
}

export const DeviceList: FunctionComponent<Props> = ({ devices, onclick }) => {
  const [devicesState, setDevicesState] = useState<Array<DeviceState>>([]);

  const checkControllerState = async (host: string): Promise<boolean> => {
    return new Promise(async (resolve) => {
      try {
        const response = await fetchTimeout(150, `http://${host}/healthz`);
        if (response.status >= 200 && response.status < 300)
          return resolve(true);
        return resolve(false);
      } catch (err) {
        return resolve(false);
      }
    });
  };

  const fetchControllerState = useCallback(async () => {
    const stateResolvers: Array<Promise<boolean>> = [];
    for (const device of devices) {
      const controllerState: Promise<boolean> = checkControllerState(device.ip);
      stateResolvers.push(controllerState);
    }

    const finished = await Promise.all(stateResolvers);

    const nd = devices.map((item, i) => {
      let onlineState: "online" | "offline" = "offline";
      if (finished[i]) {
        onlineState = "online";
      }

      return { device: item, state: onlineState };
    });

    setDevicesState(nd);
    window.localStorage.setItem(
      "availableControllers",
      JSON.stringify(
        nd.filter((item) => item.state === "online").map((item) => item.device)
      )
    );
  }, [devices]);

  useEffect(() => {
    const nD = [];
    for (const device of devices) {
      nD.push({ device, state: "offline" } as DeviceState);
    }
    setDevicesState(nD);

    const interval = setInterval(() => {
      fetchControllerState();
    }, 5 * 1000);

    return () => {
      clearInterval(interval);
    };
  }, [devices, fetchControllerState]);

  useEffect(() => {
    //if (isLoaded) return;
    /*const stateResolvers: Array<Promise<boolean>> = [];
    for (let device of devicesState) {
      (async () => {
        const controllerState: Promise<boolean> = checkControllerState(
          device.device.ip
        );
        stateResolvers.push(controllerState);
      })();
    }

    (async () => {
      const finished = await Promise.all(stateResolvers);

      const nd = devicesState.map((item, i) => {
        let onlineState: "online" | "offline" = "offline";
        if (finished[i]) {
          onlineState = "online";
        }

        return { device: item.device, state: onlineState };
      });

      setDevicesState(nd);
      window.localStorage.setItem(
        "availableItems",
        devicesState.map((item) => item.state === "online" && item).join(" ")
      );
      setIsLoaded(true);
    })();*/
  }, [devicesState]);

  return (
    <div className={styles.deviceList}>
      {devicesState.length === 0 && (
        <p>Loading last controller configuration</p>
      )}

      {devicesState.map((device, i) => (
        <div
          key={i}
          onClick={() => device.state === "online" && onclick(device.device)}
          className={styles.deviceList__item}
        >
          {device.device.ip}:{device.device.port} - {device.state}{" "}
          {device.state === "offline" && <div>Remove</div>}
        </div>
      ))}
    </div>
  );
};
