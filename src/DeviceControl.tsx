import React, {
  FunctionComponent,
  MouseEvent,
  useEffect,
  useState,
} from "react";
import { Device } from "./components/DeviceList/DeviceList";
import { FadeLedModeControl } from "./components/Modes/FadeLedModeControl";
import { InitialModeConfig, ModeControl } from "./components/Modes/ModeControl";
import { ModeControlProvider } from "./components/Modes/ModeControlProvider";
import { StaticLedModeControl } from "./components/Modes/StaticLedModeControl";
import { LEDSocketState, stringFromLEDConnectState } from "./LEDSocket";
import { useWebSocket } from "./useWebSocket";

interface Props {
  device: Device;
}

export const DeviceControl: FunctionComponent<Props> = ({ device }) => {
  const socket = useWebSocket(device.ip, device.port);

  const [mode, setMode] = useState<string>("STATICLEDMODE");
  const [powerState, setPowerState] =
    useState<InitialModeConfig["controllerState"]>("off");

  const [initialConfig, setInitialConfig] = useState<
    Partial<InitialModeConfig>
  >({});

  useEffect(() => {
    socket.subsribe("config", ({ data }) => {
      console.log(data, "Data");
      if (data.initialConfig) {
        setInitialConfig(data.initialConfig);
      }

      if (data.controllerState) {
        setPowerState(data.controllerState);
      }
    });
  }, [socket]);

  useEffect(() => {
    if (initialConfig.currentMode) {
      setMode(initialConfig.currentMode);
    }

    if (initialConfig.controllerState) {
      setPowerState(initialConfig.controllerState);
    }
  }, [initialConfig]);

  useEffect(() => {
    socket.subsribe("modeChange", ({ data }) => {
      if (data.newModeName) {
        setMode(data.newModeName);
      }
    });
  }, [socket, mode]);

  const onClickMode = (e: MouseEvent<HTMLLIElement>) => {
    const data = e.currentTarget.dataset.modename;
    if (data) socket.send("modeChange", { modeName: data });
  };

  const switchPowerState = (e: MouseEvent<HTMLSpanElement>) => {
    socket.send("config", {
      controllerState: powerState === "off" ? "on" : "off",
    });

    //setPowerState(powerState === "off" ? "on" : "off");
  };

  return (
    <div>
      <p>
        Current Device: {device.ip}:{device.port} - State:{" "}
        {stringFromLEDConnectState(socket.state)}
      </p>
      Power State: <span onClick={switchPowerState}>{powerState}</span>
      <h2>Set mode:</h2>
      <ul>
        <li data-modename="STATICLEDMODE" onClick={onClickMode}>
          Static
        </li>
        <li data-modename="FADELEDMODE" onClick={onClickMode}>
          Fade
        </li>
      </ul>
      <h2>Controls:</h2>
      {socket.state === LEDSocketState.OPEN ? (
        <ModeControlProvider
          currentMode={mode}
          currentDevice={device}
          config={initialConfig}
        >
          <ModeControl
            modeName="STATICLEDMODE"
            component={<StaticLedModeControl />}
          />
          <ModeControl
            modeName="FADELEDMODE"
            component={<FadeLedModeControl />}
          />
        </ModeControlProvider>
      ) : (
        <div>Loading Controller...</div>
      )}
    </div>
  );
};
