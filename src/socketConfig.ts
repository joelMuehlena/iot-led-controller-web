import { LEDSocket } from "./LEDSocket";

export const socketConnections: Map<string, LEDSocket> = new Map<
  string,
  LEDSocket
>();
