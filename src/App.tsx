import React, { useEffect, useState } from "react";
import { DeviceControl } from "./DeviceControl";
import { Device, DeviceList } from "./components/DeviceList/DeviceList";

//Debug fallback
const sockets: Array<{ ip: string; port: number }> = [
  { ip: "192.168.0.94", port: 81 },
  { ip: "192.168.0.93", port: 81 },
];

const deviceDiscoveryService: string = "http://localhost:8080";
const deviceDiscoveryPath: string = "/discoverDevices";

const discoverDevices = async (): Promise<Array<Device>> => {
  return new Promise(async (resolve, reject) => {
    try {
      const response = await fetch(
        `${deviceDiscoveryService}${deviceDiscoveryPath}`
      );
      console.log(response);

      if (response.status >= 200 && response.status < 300) {
        const bodyJson = await response.json();
        console.log("json", bodyJson);

        let deviceList: Array<Device> = [];

        if (bodyJson.devices) {
          deviceList = bodyJson.devices as Array<Device>;
        }

        return resolve(deviceList);
      }
      return reject(new Error("Not 200 Status code"));
    } catch (error) {
      return reject(error);
    }
  });
};

function App() {
  const [currDevice, setCurrDevice] = useState<Device | null>();
  const [allDevices, setAllDevices] = useState<Array<Device>>([]);

  useEffect(() => {
    const devices = window.localStorage.getItem("availableControllers");
    if (devices) {
      try {
        const parsed: Array<Device> = JSON.parse(devices);
        setAllDevices(parsed);
      } catch (err) {
        setAllDevices([]);
        fetchDevices();
        return;
      }
    }

    fetchDevices();
  }, []);

  const fetchDevices = async () => {
    try {
      const devices = await discoverDevices();
      console.log("discovered devices", devices);
    } catch (err) {
      console.warn("Error fetching", err);
    }
  };

  const onclickDevice = (device: Device) => {
    setCurrDevice(device);
  };

  return (
    <div className="App">
      <DeviceList devices={allDevices} onclick={onclickDevice} />

      {currDevice && <DeviceControl device={currDevice} />}
    </div>
  );
}

export default App;
